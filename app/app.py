import io
import logging
import os
import subprocess
import zipfile
from pathlib import Path
from tempfile import SpooledTemporaryFile

import bcrypt

from flask import Flask, flash, redirect, render_template, request, send_file
from flask_simplelogin import SimpleLogin, login_required
from werkzeug.exceptions import InternalServerError

app = Flask(__name__)

# read parameters, fail if missing
app.config['SECRET_KEY'] = os.environ['SECRET_KEY']
HASHED_USER_PASSWORD = os.environ['HASHED_USER_PASSWORD'].encode()

# setup logger
logger = logging.getLogger("scan")
LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()
logger.setLevel(level=LOGLEVEL)


# setup the login manager
def login_checker(user):
    if user.get('username') == 'user' and \
       bcrypt.checkpw(user.get('password').encode(), HASHED_USER_PASSWORD):
        return True
    return False


SimpleLogin(app, login_checker=login_checker)

class GpgError(Exception):
    pass

class RequestError(Exception):
    pass

def parse_request(req):
    # check uploaded files
    uploaded_files = req.files.getlist("files")

    # check if the post req has the file part
    if not uploaded_files:
        raise RequestError('No file part')

    # If the user does not select a file, the browser submits an
    # empty file without a filename.
    for file in uploaded_files:
        if file.filename == '':
            raise RequestError('One of the files has an empty name')

    return uploaded_files


@app.route("/", methods=['GET', 'POST'])
@login_required
def main():
    outcome = {'ok': None, 'err': None}

    if request.method == 'POST':
        try:
            uploaded_files = parse_request(request)

            # encrypt file using PGP
            encrypted_files = {
                file.filename: encrypt_file(file.filename, file.stream)
                for file in uploaded_files
            }

            with SpooledTemporaryFile() as docs_archive:
                with zipfile.ZipFile(docs_archive, 'w', zipfile.ZIP_DEFLATED) as zipf:
                    for filename, content in encrypted_files.items():
                        zipf.writestr(filename + '.gpg', data=content)

                docs_archive.seek(0)

                return send_file(
                    io.BytesIO(docs_archive.read()),
                    mimetype='application/zip',
                    as_attachment=True,
                    download_name='encripted.zip')

        except GpgError as ex:
            outcome['err'] = ex

        except RequestError as ex:
            outcome['err'] = ex

    return render_template('home.html', outcome=outcome)

def encrypt_file(filename, stream):
    pkey_path = '/public_key'

    if Path(pkey_path).is_file():
        command = ['/usr/bin/gpg', '--batch', '--encrypt',
                   '--recipient-file', '/public_key',
                   '--output', '-',
                   '--set-filename', filename]

        process = subprocess.run(command, stdin=stream, stdout=subprocess.PIPE, timeout=10)

        if process.stderr:
            stderr = process.stderr.decode("utf-8").replace('\n', '')
            raise InternalServerError(f"[ERROR] in GPG encryption: {stderr}")
        elif len(process.stdout) == 0:
            raise InternalServerError("[ERROR] Encrypted file is empty")
        else:
            return process.stdout
    else:
        raise InternalServerError('[ERROR] Public key is not a file: /public_key')


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=5009)
