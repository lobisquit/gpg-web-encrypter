FROM python:slim

# install system dependencies
RUN apt-get update && \
	apt-get install -y openssl gpg && \
	apt-get clean

# run everything as normal user
RUN useradd --system --create-home runner
USER runner
WORKDIR /home/runner/app

# upgrade pip to the latest-est version and install requirements
ENV PATH="/home/runner/.local/bin:${PATH}"
RUN pip install --upgrade pip

# install application dependencies
COPY app/requirements.txt requirements.txt
RUN pip install --user --no-cache-dir -r requirements.txt

# load actual application code, as a last step to
# prevent re-installs after code changes
COPY ./app /home/runner/app

VOLUME ["/public_key"]

ENTRYPOINT ["/home/runner/app/run.sh"]
